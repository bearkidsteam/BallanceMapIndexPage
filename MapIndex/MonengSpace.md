# Moneng Space

## Quick view

|||
|:---|:---|
|Map name|Moneng Space|
|Original name|魔脓空间站|
|Author|傲骨银狼ヶ璇|
|Release date|2011|
|Checkpoints' count|Unknow|
|Anthorized map|Freedom|
|Suit for|Ballance, Ballance Remix|
|Included in Map Package|Yes|
|Supported by|ScoreManager|

## Screenshots

![](../Img/MonengSpace.jpg)

## Download
None

## Videos
* [Youtube](https://youtu.be/o_pCj8kji34)
* [Youku](http://v.youku.com/v_show/id_XNjc5MTUwNzk2.html)

## Comments

**qwezxc385** comments on Chinese Ballance Forum
>很有创意的一张地图，排第一没意见
