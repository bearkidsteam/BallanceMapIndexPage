# Ballance Carnival 1.1

## Quick view

|||
|:---|:---|
|Map name|Ballance Carnival 1.1|
|Original name|Ballance嘉年华1.1|
|Author|fiype|
|Release date|2012|
|Checkpoints' count|8|
|Anthorized map|Freedom|
|Suit for|Ballance|
|Included in Map Package|Yes|
|Supported by|None|

## Screenshots

![](../Img/BallanceCarnival1.1.jpg)

## Download
None

## Videos
* [Youku](http://v.youku.com/v_show/id_XODkwNjkxNjIw.html)

## Comments

**qwezxc385** comments on Chinese Ballance Forum
>原版很多欲死不能，新的修改了很多，很好的一张图。