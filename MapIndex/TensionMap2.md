# Tension Map 2

## Quick view

|||
|:---|:---|
|Map name|Tension Map 2|
|Original name|紧张图2|
|Author|紧张2007324|
|Release date|2014|
|Checkpoints' count|Unknow|
|Anthorized map|Freedom|
|Suit for|Ballance|
|Included in Map Package|Yes|
|Supported by|None|

## Screenshots

![](../Img/TensionMap2.jpg)

## Download
None

## Videos
* [Youku](http://v.youku.com/v_show/id_XODAzNTAyMDEy.html?refer=eco-h5-zpd&tuid=UMzcxMjI0MjQw)

## Comments

**qwezxc385** comments on Chinese Ballance Forum
>技术难度最大、节奏最快的地图。首次实现了蓝色柱子的渐变。是紧张比较好的作品。