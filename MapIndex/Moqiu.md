# Moqiu

## Quick view

|||
|:---|:---|
|Map name|Moqiu|
|Original name|魔虬|
|Author|失衡之梦|
|Release date|2012|
|Checkpoints' count|8|
|Anthorized map|Freedom|
|Suit for|Ballance|
|Included in Map Package|Yes|
|Supported by|ScoreManager|

## Screenshots

![](../Img/Moqiu.jpg)

## Download
None

## Videos
* [Youtube](https://youtu.be/wXxlhX8b8L8)
* [Youku](http://v.youku.com/v_show/id_XNjc5MzMzNjM2.html?x&from=y7.2-1-99.3.1-1.17-1-1-0-0)

## Comments

**qwezxc385** comments on Chinese Ballance Forum
>很考验玩家耐心和细心的一个图，后来满屏虬字简直精神污染。。。密集恐惧的就别玩了
