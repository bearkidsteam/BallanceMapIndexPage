# Pseudo Power 4

## Quick view

|||
|:---|:---|
|Map name|Pseudo Power 4|
|Original name|伪力量4|
|Author|紧张2007324|
|Release date|2012|
|Checkpoints' count|6|
|Anthorized map|Freedom|
|Suit for|Ballance|
|Included in Map Package|Yes|
|Supported by|None|

## Screenshots

![](../Img/PseudoPower4.jpg)

## Download
None

## Videos
* [Youku](http://v.youku.com/v_show/id_XNzAxMDIwMTQw.html)

## Comments

**qwezxc385** comments on Chinese Ballance Forum
>紧张首个正式制图作品，创意和可玩性都不错，现在有4.4了
